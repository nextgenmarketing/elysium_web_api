const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
import * as tf from '@tensorflow/tfjs';
import * as tmImage from '@teachablemachine/image';
import bodyParser from 'body-parser';
/* Post send email. */
 let model, maxPredictions;

router.get('./tensorflow', function(req,res){
    res.send("test api");
});

router.post('/tensorflow',bodyParser.raw({type: ["image/jpeg","image.png"],limit: "5mb" }), function(req,res){

    init();
    const prediction = await model.predict(req.body);
    maxPredictions = model.getTotalClassess();
    let resultProbability = 0;
    let result;
    for( let i = 0;i<maxPredictions; i++){
        if (prediction[i].probability> resultProbability){
            resultProbability = preduction[i].probability;
            result = prediction[i].className;
        }
    }

    res.end(result);
    

});


async function init() {

    let URL ="../model/";
    const modelURL = URL + "model.json";
    const metadataURL = URL + "metadata.json";

    // load the model and metadata
    // Refer to tmImage.loadFromFiles() in the API to support files from a file picker
    // or files from your local hard drive
    // Note: the pose library adds "tmImage" object to your window (window.tmImage)
    model = await tmImage.load(modelURL, metadataURL);
    maxPredictions = model.getTotalClasses();

    
    // append elements to the DOM
    document.getElementById("webcam-container").appendChild(webcam.canvas);
    labelContainer = document.getElementById("label-container");
    for (let i = 0; i < maxPredictions; i++) { // and class labels
        labelContainer.appendChild(document.createElement("div"));
    }
}



module.exports = router;