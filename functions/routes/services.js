const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const servicesCRUD = require('../controllers/crud/servicesCtrl')
const httpErrorResponses = require('../controllers/httpErrorResponses')

  // Create a new service
  router.post("/", servicesCRUD.create);
  // Retrieve all services
  router.get("/", servicesCRUD.findAll);
  // Retrieve all published services
  router.get("/published", servicesCRUD.findAllPublished);
  // Retrieve a single service with id
  router.get("/:id", servicesCRUD.findOne);
  // Update a service with id
  router.post("/:id", servicesCRUD.update);
  // Delete a service with id
  router.delete("/:id", servicesCRUD.delete);
  // delete all services
  router.delete("/", httpErrorResponses.notFound);
module.exports = router;