const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const productsCRUD = require('../controllers/crud/productsCtrl')
const httpErrorResponses = require('../controllers/httpErrorResponses')

  // Create a new product
  router.post("/add", productsCRUD.create);
  // Retrieve all products
  router.get("/", productsCRUD.findAll);
  // Retrieve all published products
  router.get("/published", productsCRUD.findAllPublished);
  // Retrieve a single product with id
  router.get("/:id", productsCRUD.findOne);
  // Update a product with id
  router.get("/:id", productsCRUD.update);
  // Delete a product with id
  router.delete("/:id", productsCRUD.delete);
  // delete all products
  router.delete("/", httpErrorResponses.notFound);

module.exports = router;
