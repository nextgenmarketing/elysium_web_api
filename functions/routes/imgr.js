const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a imgr resource');
});

/**
 * POST
 * saves image recognition model to DB
 */
router.post('/save', function(req, res, next) {
  res.send('respond with a imgr resource');
});

/**
 * POST
 * retrieve's image recognition model from DB
 */
router.post('/load', function(req, res, next) {
  res.send('respond with a imgr resource');
});

module.exports = router;
