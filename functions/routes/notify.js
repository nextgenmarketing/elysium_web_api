const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const notify = require('../controllers/api/comms')

/* Post send email. */
router.post('/email', notify.sendEmail);

/* Post send sms. */
router.post('/sms', notify.sendSMS);

module.exports = router;
