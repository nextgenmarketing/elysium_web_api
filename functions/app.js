const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
//database stuff
const admin = require('firebase-admin');
const bodyparser = require('body-parser');
const functions = require('firebase-functions');

//var serviceAccount = require("../firebaseconfig.json");

admin.initializeApp({
  //credential: admin.credential.cert(serviceAccount)
});




// controllers
const fbAuthCtrl = require('./controllers/api/auth.js');
const fbStoreCtrl = require('./controllers/api/firestore.js')
const commCtrl = require('./controllers/api/comms.js')


// routes
const indexRouter = require('./routes/index');
// const usersRouter = require('./routes/users');
const productsRouter = require('./routes/products');
const servicesRouter = require('./routes/services');
const imgrRouter = require('./routes/imgr');
const notifyRouter = require('./routes/notify');
const tensorflowRouter = require('./routes/tensorflow');


const app = express();

//app.get('/', (req,res)=>res.json('hi'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(helmet());

// app.use('/users', usersRouter);
app.use('/products', productsRouter);
app.use('/services', servicesRouter);
app.use('/imgr', imgrRouter);
app.use('/notify', notifyRouter);
app.use('/tensorflow',tensorflowRouter);
app.use('/', indexRouter);

exports.app = functions.https.onRequest(app);


