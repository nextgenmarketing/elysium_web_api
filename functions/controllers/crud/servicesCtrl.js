//Create service
exports.create = async (req, res) => {
  
};
// Retrieve all services from the database.
exports.findAll = async (req, res) => {
  
};
// Find a single service with an id
exports.findOne = async (req, res) => {
  
};
// Update a service by the id in the request
exports.update = async (req, res) => {
  
};
// Delete a service with the specified id in the request
exports.delete = async (req, res) => {
  
};
// Delete all services from the database.
exports.deleteAll = async (req, res) => {
  
};
// Find all published services
exports.findAllPublished = async (req, res) => {
  
};