const firestore = require('../api/firestore')
const auth = require('../api/auth');
//const dbRef = "products"
//Create product
exports.create = async (req, res) => {
    try {
        const {token, newData} = req.body
        const uid =  await auth.verifyIdToken(token)
        await firestore.writeDB(dbRef, uid, newData)
        .then(result => {
          console.log(result.id)
          res.status(200).send({
              code: 200,
            message:
               `200 - product added - product ID : ${result.id}`
          });
        })
      } catch (err) {
        console.log(`Faild to add product. Error Message is: ${err.message}`);
        res.status(404).send({
            code : 400,
          message:
             `404 - create product failed. Err : ${err}`
        });
      }
};
// Retrieve all products from the database.
exports.findAll = async (req, res) => {
    try {
        const {user, dbRef} = req.query
        await firestore.readDB(dbRef, user)
        .then(result => {
            if (result == 400 ) {
                res.status(200).send({
                    code: 200,
                    message:
                     `no products avaiable`,
                     data : {}
                });
            } else {
                console.log(result.id)
                res.status(200).send({
                    code: 200,
                    message:
                     `200 - product added - product ID : ${result.id}`,
                     data : result
                });
            }

        })
      } catch (err) {
        console.log(`Faild to add product. Error Message is: ${err.message}`);
        res.status(404).send({
            code : 400,
          message:
             `404 - create product failed. Err : ${err}`
        });
      }
};
// Find a single product with an id
exports.findOne = async (req, res) => {
    const id = req.params['id']
};
// Update a product by the id in the request
exports.update = async (req, res) => {
  
};
// Delete a product with the specified id in the request
exports.delete = async (req, res) => {
  
};
// Delete all products from the database.
exports.deleteAll = async (req, res) => {
  
};
// Find all published products
exports.findAllPublished = async (req, res) => {
  
};