exports.notFound = async (req, res) =>{
    res.status(404).send({
        message:
           "404 - Page Not Found."
      });
}

exports.missingParam = async (req, res) =>{
    res.status(404).send({
        message:
           `404 - missing param ${req}`
      });
}