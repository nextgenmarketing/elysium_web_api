//require("dotenv").config();
const admin = require('firebase-admin');
 
const auth = admin.auth();




 /**
* This is a description
* @method verifyIdToken
* @param {String} token string
* @return {Object} decodedToken
*/

exports.verifyIdToken = function(token) {

     return auth.verifyIdToken(token)
    .then(function(decodedToken) {
      let uid = decodedToken.uid;
      // ...
      //console.log(decodedToken)
     return decodedToken;
    }).catch(function(error) {
      // Handle error
      console.error(error)
        return error;
    });
}

