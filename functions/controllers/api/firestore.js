// require("dotenv").config();
const admin = require('firebase-admin');
const db = admin.firestore();
// USER DB

 /**
* reads the DB
* @method readDB
* @param {String} dbRef name of db to read
* @param {String} user account uid 
* @return {Object | Number} doc data || 400 depending on pass or fail
*/

exports.readDB = async function(dbRef, user){
  console.info("read: ", dbRef, user)
 
    await db.collection(dbRef).doc(user).get()
    .then(data => {
      if (!data.exists) {
        console.log('No such document!');
        return 400;
      } 
      return  data.data();
    })
    .catch(error => {
      console.error(error)
    })
}


 /**
* write to DB
* @method writeDB
* @param {String} dbRef name of db to read
* @param {String} user account uid 
* @param {Object} newData things to add to db
* @return {Object | Number} doc data || 400 depending on pass or fail
*/

exports.writeDB = async function(dbRef, user, newData){
    console.debug(`writeDB params:`, dbRef, user, newData)
    const obj = JSON.parse(newData);
    return await db.collection(dbRef).doc(user).set(obj)
    .then( async (result)=>{
      console.log('result.id: ', result);
      return result;
  
    }).catch((error)=>{
      console.log(error);
      return error;
    });

  }


 /**
* Updates the DB
* @method updateDB
* @param {String} user account uid 
* @param {Object} newData info to be updated in user
* @return {Number} 200 || 400 depending on pass or fail
*/

exports.updateDB = async function(dbRef, user, newData){

    return await db.collection(dbRef).doc(user).update(newData)
    .then( res => {
        console.log('res from updateDb: ', res)
        return 200;
    })
    .catch(err => {
        console.error(err);
        return 400;
    }) 

}


 /**
* query the DB
* @method queryDB
* @param {Number} queryNum number [1-3] of compound .where statements 
* @param {Array} searchParams list of params read off in an order
* @return {Number} 200 || 400 depending on pass or fail
*/

exports.queryDB = async function(queryNum, searchParams){

    if (searchParams.length / queryNum !== 3) {
        return 'insufficent search params'
    } 
    let snapshot;

    switch (queryNum) {
        case 1: 
        snapshot = await usrDB
                            .where(searchParams[0], searchParams[1], searchParams[2])
                            .get();
            break;
        case 2:
        snapshot = await usrDB 
                            .where(searchParams[0], searchParams[1], searchParams[2])
                            .where(searchParams[3], searchParams[4], searchParams[5])
                            .get();
            break;
        case 3: 
        snapshot = await usrDB
                            .where(searchParams[0], searchParams[1], searchParams[2])
                            .where(searchParams[3], searchParams[4], searchParams[5])
                            .where(searchParams[6], searchParams[7], searchParams[8])
                            .get();
            break;
    }
    console.log(snapshot)
    if (snapshot.empty) {
    console.log('No matching documents.');
    return 'no matching documents'
  }  
  
  const list = [];
  snapshot.forEach(doc => {
    list.push(doc.data())
  //console.log(doc.id, '=>', doc.data());
  });

  return list;

}

