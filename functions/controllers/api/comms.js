//require("dotenv").config();

const env = require('../../ignore/env.js');
const httpErrorResponses = require('../httpErrorResponses')
//Twilio
const smsClient = require('twilio')(env.TWILIO_ACCOUNT_SID, env.TWILIO_AUTH_TOKEN);


//SendGrid
// const SENDGRID_KEY="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";
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey((env.SENDGRID_KEY));




 /**
* send email using sendgrid
* @method sendEmail
* @param {Object} message 
* @return {Number} 200 || 400 / pass || fail
*/

  exports.sendEmail = async (req, res) => {

    const {to, text, subject, html} = req.body
    if (!to ||  !subject || !text || !html) {
      res.status(404).send({
        code : 404, 
        message:
           `Email failed. Missing Params { To: ${!to}, Subject : ${!subject}, ` + 
           `Text: ${!text}, Html: ${!html} }`
      })
    }
    const msg = {
      to: req.body.to,
      from: 'support@elysianfields.com',
      subject: req.body.subject,
      text: req.body.text,
      html: req.body.html ,
    };

    //console.log(msg);
    try {
        await sgMail.send(msg)
        console.log('Mail has been sent successfully.');
        res.status(200).send({
          message:
             `200 - Email sent`
        });
      } catch (err) {
        console.log(`Mail Send Error. Error Message is: ${err.message}`);
        res.status(404).send({
          message:
             `404 - Email failed. Err : ${err}`
        });
      }
  
  }
  

   /**
* send sms via twilio
* @method sendSMS
* @param {Object} message 
* @return {Number} 200 || 400 / pass || fail
*/
  exports.sendSMS = async (req, res) => {
  
    const {to, body} = req.body

    if (!body ||  !to) {
      res.status(404).send({
        code : 404, 
        message:
           `Sms failed. Missing Params { To: ${!to}, Message Body : ${!body} }`
      })
    }

    const newMessage = {
      body: req.body.body,
      from: '+17406395949',
      to: req.body.to
    }
    try {
        await smsClient.messages
        .create(newMessage)
        .then(message => {
          console.log(message.sid)
          res.status(200).send({
            message:
               `200 - Sms sent`
          });
        })
      } catch (err) {
        console.log(`SMS Send Error. Error Message is: ${err.message}`);
        res.status(404).send({
          message:
             `404 - Sms failed. Err : ${err}`
        });
      }
  }